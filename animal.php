<?php

class Animal {
    public $name;
    public $legs;
    public $cold_blooded;

    // public function __construct($string) {
    //     $this->name = $string;
    // }

    public function getName($name) {
        $this->name = $name;
        return $name;
    }
    public function getLegs($leg) {
        $this->legs = $leg;
        return $leg;
    }
    public function isColdBlooded($bold) {
        $this->cold_blooded = $bold;
        return $bold;
    }
}

?>