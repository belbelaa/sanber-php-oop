<?php

require_once('animal.php');
require_once('frog.php');
require_once('ape.php');

$object = new Animal;

echo "Nama : " . $object->getName("Shaun") . "<br>"; // "shaun"
echo "Legs : " . $object->getLegs("2") . "<br>"; // 2
echo "Is Cold Blooeded : " . $object->isColdBlooded("False") . "<br> <br>"; // false

$object2 = new Frog;

echo "Nama : " . $object2->getName("Katak") . "<br>"; // "shaun"
echo "Legs : " . $object2->getLegs("2") . "<br>"; // 2
echo "Is Cold Blooeded : " . $object2->isColdBlooded("False") . "<br>"; // false
echo  $object2->jump() . "<br> <br>";

$object3 = new Ape;

echo "Nama : " . $object3->getName("Ape") . "<br>"; // "shaun"
echo "Legs : " . $object3->getLegs("4") . "<br>"; // 2
echo "Is Cold Blooeded : " . $object3->isColdBlooded("True") . "<br>"; // false
echo  $object3->yell() . "<br> <br>";

?>