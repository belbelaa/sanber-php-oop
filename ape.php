<?php

// Perhatikan bahwa Frog (katak) merupakan hewan berkaki 4, hingga dia tidak menurunkan sifat jumlah kaki 2. class Ape memiliki function yell() yang mengeprint “Auooo” dan class Frog memiliki function jump() yang akan mengeprint “hop hop”.

require_once('animal.php');

class Ape extends Animal {
    public function yell() {
        echo "suara Auooo";
    }
}

?>